# @file $HOME/.bashrc
#
# Sets user environment variables and drops into fish.
#
# @author J. Nolan Faught
# @date 4 March 2023

# Set GnuPG agent tty
export GPG_TTY=$(tty)
# Disable virtualenv prompt
export VIRTUAL_ENV_DISABLE_PROMPT=1

# Start fish if interactive, otherwise evaluate the passed string
if [[ $- != *i* ]]; then
	return
else
	exec fish
fi
