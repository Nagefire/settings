-- Use treesitter for folding
vim.wo.foldmethod = "expr"
-- Set tabs to soft 4-space
vim.bo.tabstop = 4
vim.bo.softtabstop = 4
vim.bo.shiftwidth = 4
vim.bo.expandtab = true
-- Set text and column width to NvChad defaults
vim.bo.textwidth = 120
vim.wo.colorcolumn = "120"
